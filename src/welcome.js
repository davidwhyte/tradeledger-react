import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container, Row, Col} from "reactstrap";
import './welcome.css';
import Skyline from './data/london-city.jpg'

class Welcome extends React.Component {
    render(){
        return(
            <Container className="welcome">
                <Row>
                    <Col>
                        <div className="inner">
                            <div className="title-wrapper"><div className="title">Welcome! To get started, please enter a company name or ABN in the search box above</div></div>
                            <img className="skyline" src={Skyline}  alt="skyline" />
                        </div>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default Welcome;