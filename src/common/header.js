import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container, Row, Col} from "reactstrap";
import './header.css';

class Header extends React.Component {

    render(){
        return(
            <Container className="header mt-5">
                <Row>
                    <Col>
                        <header>Company Lookup System</header>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default Header;