import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container, Row, Col} from "reactstrap";
import './footer.css';

class Footer extends React.Component {
    render(){
        return(
            <Container className="footer">
                <Row>
                    <Col>
                        <footer className="inner">Created By David Whyte</footer>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default Footer;