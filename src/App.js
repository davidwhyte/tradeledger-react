import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import {Container} from "reactstrap";
import Search from './search';
import Footer from "./common/footer";
import Header from "./common/header";

function App() {

  return (
      <Container className="App">

          <Header />
          <Search />
          <Footer />

      </Container>
  );
}

export default App;
