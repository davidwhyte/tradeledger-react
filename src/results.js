import React, { useState } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import './results.css';
import {Button, Col, Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap";
import Container from "reactstrap/es/Container";

class Results extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            term: "",
            companies: [],
            json: [],
            modal: false,
            modalData: {}
        };

        this.toggle = this.toggle.bind(this);
    }

    /* Checks if results props needs updating again */
    static getDerivedStateFromProps(nextProps, prevState){
        if(nextProps.results !== prevState.results){
            return { results: nextProps.results };
        }
        else return null;
    }

    /* Prevent duplicate props from firing update */
    componentDidUpdate(prevProps, prevState) {
        if(prevProps.results !== this.props.results){
            this.updateStateFromProps();
        }
    }

    /* update state using props to update component */
    updateStateFromProps(){
        if(this.props !== null && this.props.results !== null) {
            let results = this.props.results;
            if (results && results.term && results.json) {
                this.setState((state, props) => (
                    {
                        term: String(props.results.term),
                        json: props.results.json
                    }), () => {
                    this.jsonParser()
                });

            }
        }
    }

    /* extract company objects from name results */
    jsonParser() {
        let companiesExtractedArray = [];
        if(this.state.json.Names) {

            let companyNames = this.state.json.Names;
            if(!this.showAllRequested(companyNames)){
                for (let company of companyNames) {
                    if (this.filterByTerm(this.state.term, company)) {
                        companiesExtractedArray.push(company);
                    }
                }
                this.setState({companies: companiesExtractedArray});
            }
        }

    }

    /* show all list of companies*/
    showAllRequested(companyNames){
        if(this.state.term.length === 0){
            this.setState({companies: companyNames});
            return true;
        }
        return false;
    }

    /* filter company by name and Abn using term*/
    filterByTerm(term, company){
        if((company.Name && company.Name.toString().toLowerCase().includes(term.toLowerCase())) ||
            (company.Abn && company.Abn.toString().toLowerCase().includes(term.toLowerCase()))) {
            return true;
        }
    }

    /* Show and hide the modal window.
    Also put selected company into state variable. */
    toggle(data) {
        let modalData = {};
        if(data){
            modalData = data;
        }
        this.setState({
            modal: !this.state.modal,
            modalData: modalData
        });
    }

    /* layout */
    render(){
        return (
            <Container className="results">
                <Row className="mb-5 ml-2 mr-2 mt-4">
                    <Col className="result-info">Showing <b>{this.state.companies.length}</b> results for "<b>{this.state.term}</b>"</Col>
                </Row>
                {this.state.companies.map((company, i) => {
                    return (
                        <Row className="company" key={i} >
                            <Col><b>Name:</b> {company.Name}</Col>
                            <Col><b>Abn:</b> {company.Abn}</Col>
                            <Col xs="auto"><Button onClick={() => this.toggle(company)}>Show Details</Button></Col>
                        </Row>
                    )
                })}

                <Modal isOpen={this.state.modal}>
                    <ModalHeader>Company: {this.state.modalData.Name && <span>{this.state.modalData.Name}</span>}</ModalHeader>
                    <ModalBody>
                        <Container>
                            <Row className="mb-2">
                                <Col><b>Name:</b>{this.state.modalData.Name && <span> {this.state.modalData.Name}</span>}</Col>
                            </Row>
                            <Row className="mb-2">
                                <Col><b>Name Type:</b>{this.state.modalData.NameType && <span> {this.state.modalData.NameType}</span>}</Col>
                            </Row>
                            <Row className="mb-2">
                                <Col><b>ABN:</b>{this.state.modalData.Abn && <span> {this.state.modalData.Abn}</span>}</Col>
                            </Row>
                            <Row className="mb-2">
                                <Col><b>ABN Status:</b>{this.state.modalData.AbnStatus && <span> {this.state.modalData.AbnStatus}</span>}</Col>
                            </Row>
                            <Row className="mb-2">
                                <Col><b>Is Current:</b>{this.state.modalData.IsCurrent && <span> {this.state.modalData.IsCurrent.toString()}</span>}</Col>
                            </Row>
                        </Container>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.toggle}>Close</Button>{' '}
                    </ModalFooter>
                </Modal>

            </Container>
        );
    }

}

export default Results;