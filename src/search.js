import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import './search.css';
import {Button, Col, Row} from "reactstrap";
import Container from "reactstrap/es/Container";
import Results from "./results";
import localJsonData from './data/jsonLocalData';
import Welcome from "./welcome";

class Search extends React.Component{

    constructor(props){
        super(props);

        this.guid = "b6242120-5bce-4b10-9839-d3045a7682da";

        /* default state*/
        this.state = {
            term: "",
            offline: true,
            instantFilter: true,
            results: null,
            firstTime: true
        };

        /* handler function bindings */
        this.handleTermChange = this.handleTermChange.bind(this);
        this.handleOfflineChange = this.handleOfflineChange.bind(this);
        this.handleFilterChange = this.handleFilterChange.bind(this);
        this.handleSearchClick = this.handleSearchClick.bind(this);

    }


    /* Updates term state when search term changed.
    Trigger instant filtering if enabled. */
    handleTermChange(event) {
        this.setState({
            firstTime: false,
            term: event.target.value.toString()
        }, () => {
            if(this.state.instantFilter === true){
                this.handleSearchClick();
            }
        });
    }

    /* updates offline state with checkbox status */
    handleOfflineChange(event) {
        this.setState({
            offline: event.target.checked
        })
    }

    /* updates filter state with checkbox status */
    handleFilterChange(event) {
        this.setState({
            instantFilter: event.target.checked
        });
    };

    /*
    Handles Search button click and triggers fetch.
    Uses state to determine which search type to run. */
    handleSearchClick(){
        let term = this.state.term;
        if(this.state.offline === true){
            this.fetchOfflineJson(term);
        }
        else{
            this.setState({results: null}, //clear last results
                () => {
                if(!this.searchByAbn(term)){
                    this.searchByName(term);
                }
            });
        }
    }

    /* Search by company name.
    This is default search type. */
    searchByName(term){
        let url = "https://abr.business.gov.au/json/MatchingNames.aspx?name=" + term + "&maxResults=10&callback=callback&guid=" + this.guid;
        this.fetchOnlineJson(url, term);
    }

    /* Search by company ABN.
    ABN is 11 digits.
    Function returns boolean */
    searchByAbn(term){
        let regexNumOnly = /^[0-9]*$/g;
        let url = "https://abr.business.gov.au/json/AbnDetails.aspx?abn=" + term + "&maxResults=10&callback=callback&guid=" + this.guid;
        if(term.match(regexNumOnly) && term.length === 11) {
            this.fetchOnlineJson(url, term);
            return true;
        }
        return false;
    }

    /* Fetch JSON from API.
    This occurs asynchronously.
    Promise ensures HTTP response updates state. */
    fetchOnlineJson(url, term){
        fetch(url)
            .then(res => res.text())
            .then(
                (result) => {
                    let json = this.jsonpToJson(result);
                    this.setState({ firstTime: false, results:
                        {term: term, json: json}
                    });
                },
                (error) => {
                    this.setState(
                        {firstTime: false, results:
                                {term: term, json: null, error}
                        });
                });
    }

    /* fetch json from local file */
    fetchOfflineJson(term){
        let showAll = false;
        if(term.length === 0 && this.state.instantFilter === true){
            showAll = true;
        }
        this.setState({ firstTime: false, results:
                { term: this.state.term.toString(), showAll: showAll, json: localJsonData }
        });
    }

    /*strip padding from jsonp string */
    jsonpToJson(jsonp){
        let regexStripParentheses = /\(([^()]+)\)/g;
        let jsonStr =  jsonp.match(regexStripParentheses);
        return JSON.parse(jsonStr);
    }

    /* render layout */
    render() {
        return(
            <Container className="search mb-5">

                <div className="wrapper mb-5">

                    <Row className="mt-5">
                        <Col className="input-group textbox-group">
                            <input className="form-control textbox" type="text" name="search" placeholder="Search by Name or ABN" value={this.state.term} onChange={this.handleTermChange}/>
                            <Button className="ml-3" onClick={this.handleSearchClick}>Search</Button>
                        </Col>
                    </Row>

                    <Row className="m-3">
                        <Col className="input-group options-group">
                            <label className="text-black mr-5"><input type="checkbox" name="offlineMode" value="offline" checked={this.state.offline} onChange={this.handleOfflineChange} />Offline Mode</label>
                            <label className="text-black"><input type="checkbox" name="filterMode" value="filter" checked={this.state.instantFilter} onChange={this.handleFilterChange} />Instant Filter Mode</label>
                        </Col>
                    </Row>

                </div>

                {this.state.firstTime === true && <Welcome />}
                {this.state.firstTime !== true && <Results results={this.state.results} />}

            </Container>
        );
    }

}

export default Search;